CREATE TABLE phone_book
(
  id BIGINT,
  first_name VARCHAR,
  second_name VARCHAR,
  street VARCHAR,
  phone_number BIGINT
);