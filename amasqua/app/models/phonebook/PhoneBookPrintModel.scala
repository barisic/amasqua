package models.phonebook

import amasqua.module.phonebook.service.entity.ServicePhoneBookItem

case class PhoneBookPrintModel
(
  items     : Seq[ServicePhoneBookItem],
  printTitle: String
)

