package models.phonebook

import amasqua.module.phonebook.service.entity.ServicePhoneBookItem


case class PhoneBookModel
(
  items: Seq[ServicePhoneBookItem]
)
