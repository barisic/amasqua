package models.phonebook

import amasqua.module.phonebook.service.entity.ServicePhoneBookItemCreate
import play.api.data.Forms._
import play.api.data.Form


case class PhoneBookItemCreateModel
(
  form: Form[ServicePhoneBookItemCreate]
)

object PhoneBookItemCreateForm
{
  val form: Form[ServicePhoneBookItemCreate] = Form(

    mapping(
        "id"          -> longNumber,
        "firstName"    -> text,
        "secondName"  -> text,
        "street"      -> text,
        "phoneNumber" -> longNumber
    )(ServicePhoneBookItemCreate.apply)(ServicePhoneBookItemCreate.unapply)
  )


}
