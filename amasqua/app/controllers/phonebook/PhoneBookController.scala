package controllers.phonebook

import play.api.mvc.{Action, Controller}
import views.html
import amasqua.module._shared.service.ServiceRegistry
import models.phonebook._

object PhoneBookController extends Controller
{
  val phoneBookItemsService = ServiceRegistry.PhoneBook.phoneBookItemsService

  def showPhoneBook = Action {

    implicit request =>

    val phoneBookItems = phoneBookItemsService.getAllPhoneBookItems

    Ok(html.phonebook.phoneBookItemsList(PhoneBookModel(phoneBookItems)))
  }

  def phoneBookPrintHtmlPage = Action {


      val phoneBookItems = phoneBookItemsService.getAllPhoneBookItems
      val printTitle     = "Phonebook printable HTML"

      Ok(html.phonebook.phoneBookItemsPrint(PhoneBookPrintModel(phoneBookItems, printTitle)))
  }

  def showPhoneBookItemCreatePage = Action {

    val phoneBookItemCreateForm = PhoneBookItemCreateForm.form

    Ok(html.phonebook.phoneBookItemCreate(PhoneBookItemCreateModel(phoneBookItemCreateForm)))
  }

  def create = Action {

    implicit request =>

    val phoneBookItemCreateForm = PhoneBookItemCreateForm.form

    phoneBookItemCreateForm.bindFromRequest()(request).fold(

      formWithErrors =>
      {
        BadRequest(html.phonebook.phoneBookItemCreate(PhoneBookItemCreateModel(formWithErrors)))
      },

      phoneBookItem =>
      {
        val generatedId = phoneBookItemsService.create(phoneBookItem)

        Redirect(controllers.phonebook.routes.PhoneBookController.showPhoneBook).withHeaders("id" -> generatedId.id.toString)
      }

    )

  }
}
