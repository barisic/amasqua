package amasqua

case class GeneratedId
(
  id: Long
)
