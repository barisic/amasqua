package amasqua.module.phonebook.dao.implementation

import amasqua.module.phonebook.dao.PhoneBookItemsDao
import amasqua.module.phonebook.service.entity.{ServicePhoneBookItemCreate, ServicePhoneBookItem}
import amasqua.datasource.DataSource
import play.api.db.DB
import anorm._
import play.api.Play.current
import anorm.SqlParser._
import amasqua.GeneratedId

class PhoneBookItemsDaoImpl
(
  private val dataSource: DataSource
) extends PhoneBookItemsDao
{

  private val INSERT_QUERY = SQL(
    """
      |INSERT INTO phone_book
      |
      |           (id, first_name, second_name, street, phone_number)
      |
      |        VALUES
      |
      |          ({id}, {firstName}, {secondName}, {street}, {phoneNumber})
    """.stripMargin)

  private val FIND_ALL_QUERY = SQL(
    """
      |SELECT *
      |FROM phone_book
    """.stripMargin)

  def insert(item: ServicePhoneBookItemCreate): GeneratedId =
  {
    DB.withConnection(dataSource.getName) {
      implicit connection =>

        val clientGeneratedId :Option[Long] = INSERT_QUERY.on(
                                                                "id"          -> item.id,
                                                                "firstName"   -> item.firstName,
                                                                "secondName"  -> item.secondName,
                                                                "street"      -> item.street,
                                                                "phoneNumber" -> item.phoneNumber
                                                              ).executeInsert()

        GeneratedId(clientGeneratedId.get)
    }
  }

  def findAllPhoneBookItems: Seq[ServicePhoneBookItem] = {
    DB.withConnection(dataSource.getName) {

      implicit connection =>

        FIND_ALL_QUERY.as(PhoneBookItemRowMapper.item.*)
    }
  }

  object PhoneBookItemRowMapper {
    val item = {
        get[Long]("id") ~
        get[String]("first_name") ~
        get[String]("second_name") ~
        get[String]("street") ~
        get[Long]("phone_number") map {
        case
            id ~
            firstName ~
            secondName ~
            street ~
            phoneNumber
        =>
          ServicePhoneBookItem(
            id          = id,
            firstName   = firstName,
            secondName  = secondName,
            street      = street,
            phoneNumber = phoneNumber
          )
      }
    }
  }
}
