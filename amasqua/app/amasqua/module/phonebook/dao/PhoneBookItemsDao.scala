package amasqua.module.phonebook.dao

import amasqua.module.phonebook.service.entity.{ServicePhoneBookItemCreate, ServicePhoneBookItem}
import amasqua.GeneratedId

trait PhoneBookItemsDao
{
  def insert(item: ServicePhoneBookItemCreate): GeneratedId
  def findAllPhoneBookItems:Seq[ServicePhoneBookItem]
}
