package amasqua.module.phonebook.service

import amasqua.module.phonebook.service.entity.{ServicePhoneBookItemCreate, ServicePhoneBookItem}
import amasqua.GeneratedId

trait PhoneBookItemsService
{
  def create(item: ServicePhoneBookItemCreate): GeneratedId
  def getAllPhoneBookItems: Seq[ServicePhoneBookItem]
}
