package amasqua.module.phonebook.service.entity

case class ServicePhoneBookItem
(
  id          :Long,
  firstName   :String,
  secondName  :String,
  street      :String,
  phoneNumber :Long
)
