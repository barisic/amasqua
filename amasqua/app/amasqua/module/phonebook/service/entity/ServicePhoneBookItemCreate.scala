package amasqua.module.phonebook.service.entity

case class ServicePhoneBookItemCreate
(
  id          : Long,
  firstName   : String,
  secondName  : String,
  street      : String,
  phoneNumber : Long
)


