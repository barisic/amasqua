package amasqua.module.phonebook.service.implementation

import amasqua.module.phonebook.service.PhoneBookItemsService
import amasqua.module.phonebook.service.entity.{ServicePhoneBookItemCreate, ServicePhoneBookItem}
import amasqua.module.phonebook.dao.PhoneBookItemsDao
import amasqua.GeneratedId

class PhoneBookItemsServiceImpl(private val clientsDao: PhoneBookItemsDao) extends PhoneBookItemsService
{
  def create(item: ServicePhoneBookItemCreate): GeneratedId =
  {
    clientsDao.insert(item)
  }

  def getAllPhoneBookItems: Seq[ServicePhoneBookItem] =
  {
    clientsDao.findAllPhoneBookItems
  }
}
