package amasqua.module._shared.service

import amasqua.datasource.DataSource
import amasqua.module.phonebook.dao.PhoneBookItemsDao
import amasqua.module.phonebook.dao.implementation.PhoneBookItemsDaoImpl
import amasqua.module.phonebook.service.PhoneBookItemsService
import amasqua.module.phonebook.service.implementation.PhoneBookItemsServiceImpl

object ServiceRegistry
{
  object PhoneBook
  {
    private val phoneBookItemsDao  :PhoneBookItemsDao     = new PhoneBookItemsDaoImpl(DataSource.DEFAULT)
    val phoneBookItemsService      :PhoneBookItemsService = new PhoneBookItemsServiceImpl(phoneBookItemsDao)
  }
}
