import com.typesafe.sbt.SbtNativePackager.packageArchetype
import play.PlayScala
import sbt._
import Keys._
import scala._

packageArchetype.java_application

name := """amasqua"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "com.twitter" % "finagle-http_2.10" % "6.18.0",
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc4"
)
